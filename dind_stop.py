#!/usr/bin/python3

import subprocess as sp


def dind(docker="docker", container_name="dind", image="docker:19.03.1-dind", subnet="172.25.0.0/16", network_name="dind-net"):
    sp.check_call([docker, "stop", container_name])
    sp.check_call([docker, "rm", container_name])
    sp.check_call([docker, "network", "rm", network_name])


if __name__ == "__main__":
    dind()
