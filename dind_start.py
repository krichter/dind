#!/usr/bin/python3

import subprocess as sp


def dind(docker="docker", container_name="dind", image="docker:19.03.1-dind", subnet="172.25.0.0/16", network_name="dind-net"):
    sp.check_call([docker, "network", "create", "--subnet", subnet, network_name])
    sp.check_call([docker, "run", "-d", "--name", "dind", "--privileged", "--restart", "always", "--network", network_name, "-v", "/var/lib/docker", "-e", "DOCKER_TLS_CERTDIR=", image, "--storage-driver=overlay2"])


if __name__ == "__main__":
    dind()
